import { Ingredient } from '../../shared/ingredient.model';
import { Action } from '@ngrx/store';
import * as ShoppingListActions from './shopping-list.actions';


export interface State {
    ingredients: Ingredient[];
    editedIngredient: Ingredient;
    editedIngredientIndex: number;
}

const initialState: State = {
    ingredients: [
        new Ingredient('Apples', 5),
        new Ingredient('Tomatoes', 10)
    ],
    editedIngredient: null,
    editedIngredientIndex: -1
};

export function shoppingListReducer(state: State = initialState, action: any) {
    switch (action.type) {
        case ShoppingListActions.ADD_INGREDIENT:
            // Make a copy of the inital state and add new state variables when an ingredient
            // is add. Never touch the initial state.
            return {
                ...state,
                ingredients: [...state.ingredients, action.payload]
            };
        case ShoppingListActions.ADD_INGREDIENTS:
            // Make a copy of the inital state and add new state variables when ingredients are added.
            // Never touch the initial state.
            return {
                ...state,
                ingredients: [...state.ingredients, ...action.payload]
            };
        case ShoppingListActions.UPDATE_INGREDIENT:
            // Get the ingredient to be updated from the state
            const ingredientToBeUpdated = state.ingredients[state.editedIngredientIndex];
            const updatedIngredient = {
                ...ingredientToBeUpdated,       // Has name and amount
                ...action.payload    // Overwrite it with the ingredient from the payload
            };
            // Get a copy of all the ingredients in the state and update the required index with the above computed updatedIngredient
            const updatedIngredients = { ...state.ingredients };
            updatedIngredients[state.editedIngredientIndex] = updatedIngredient;
            return {
                ...state,
                ingredients: updatedIngredients,
                editedIngredientIndex : -1,
                editedIngredient: null

            };
        case ShoppingListActions.DELETE_INGREDIENT:
            // Filter the ingredients array where the index is not equal to payload(number)
            return {
                ...state,
                ingredients: state.ingredients.filter((ig, igIndex) => {
                    return igIndex !== state.editedIngredientIndex;
                })
            };
        case ShoppingListActions.START_EDIT:
            return {
                ...state,
                editedIngredientIndex: action.payload,
                editedIngredient: {...state.ingredients[action.payload]}
            };
        case ShoppingListActions.STOP_EDIT:
            return {
                ...state,
                editedIngredient: null,
                editedIngredientIndex: -1
            };
        default:
            return state;
    }
}
