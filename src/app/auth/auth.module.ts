import { NgModule } from "@angular/core";
import { AuthComponent } from './auth.component';
import { SharedModule } from "../shared/shared.module";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";

const authRoutes = [
    { path: '', component: AuthComponent }
]

@NgModule({
    declarations: [AuthComponent],
    imports: [
        SharedModule,
        RouterModule.forChild(authRoutes),
        FormsModule
    ],
    exports: []
})
export class AuthModule {

}